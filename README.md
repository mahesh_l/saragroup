# saragroup

clone the repo

import sara.sql (available in the root folder : saragroup/sara.sql) file to create DB

Verify the below db credentials . You can update it in src/Models/DB.php if required 
PORT = '3306';
USER = 'root';
PASS = '';
DB_NAME = 'sara';

cd saragroup/

composer update

open browser with http://localhost/saragroup

Post man collection link https://www.getpostman.com/collections/e3c429b5beae77c0772b

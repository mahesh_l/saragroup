<?php
require_once "./vendor/autoload.php";
require_once "./src/config/constants.php";

$request = $_SERVER['REQUEST_URI'];
$array = explode("/", $request);
$id = intval(end($array));

$router = [
    BASE_URL => 'App',
    API_BASE_URL.'/user' => 'API',
    API_BASE_URL.'/addUserCourse' => 'API',
    API_BASE_URL.'/updateUserCourse' => 'API',
    API_BASE_URL.'/getAllUser' => 'API',
    API_BASE_URL.'/getAllUserCourse' => 'API',
    API_BASE_URL.'/course' => 'API'
];

function checkRouting($router, $requestUri){
    if (isset($router[$requestUri])){
        return true;
    }

    return false;
}

if(checkRouting($router, $request)){
    if ($request == BASE_URL){
        $home = new \App\Controllers\HomeController();
        $home->index();
    }

    if ($request == API_BASE_URL.'/user'){
        $apiClient = new \App\Services\ApiClient;
        $apiClient::User('addUser');
    }

    if ($request == API_BASE_URL.'/addUserCourse'){
        $apiClient = new \App\Services\ApiClient;
        $apiClient::User('addUserCourse');
    }

    if ($request == API_BASE_URL.'/updateUserCourse'){
        $apiClient = new \App\Services\ApiClient;
        $apiClient::User('updateUserCourse');
    }
    
    if ($request == API_BASE_URL.'/getAllUser'){
        $apiClient = new \App\Services\ApiClient;
        $apiClient::User('getAllUser');
    }

    if ($request == API_BASE_URL.'/getAllUserCourse'){
        $apiClient = new \App\Services\ApiClient;
        $apiClient::User('getAllUserCourse');
    }

    if ($request == API_BASE_URL.'/course'){
        $apiClient = new \App\Services\ApiClient;
        $apiClient::Course('addCourse');
    }
}else{
    $home = new \App\Controllers\BaseController();
    $home->errorNotFind();
}
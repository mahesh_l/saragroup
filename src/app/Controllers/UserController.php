<?php

namespace App\Controllers;

use App\Services\User;
use App\Utils\Request;

class UserController extends BaseController{

    public function __construct(){

    }

    public function index(){
        $message =  array('Hello', 'Welcome');
        $this->doRender('home/index', $message);
    }

    public function adUser(){
        //echo json_encode(Request::postParams());
        $input = Request::postParams();
        $options = [];
        if(empty($input['UserID']) || empty($input['Name']) || empty($input['Username']) || empty($input['Password']) || empty($input['Gender']) || empty($input['JoinedDate'])){
            $this->sendClientValidationError();

        }else{
            $user = new User('addUser');
            var_dump($user);
            //$this->sendJson($input);
        }
    }
}
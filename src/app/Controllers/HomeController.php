<?php

namespace App\Controllers;

use App\Views\View;

class HomeController extends BaseController{
    private $crawler;

    public function __construct(){

    }

    public function index(){
        $message =  array('Hello', 'Welcome');
        $this->doRender('home/index', $message);
    }
}
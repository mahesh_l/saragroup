<?php

namespace App\Controllers;

use App\Views\View;

class BaseController{

	public function doRender($pathName, $data) {
	    View::render($pathName, $data);
	}

	public function sendJson($data) {
	    header('Content-Type', 'application/json');
	    echo json_encode($data);
	}

	public function errorNotFind (){
        $message = array('404', 'Not found');
        $this->doRender('message', $message);
    }

    public function sendClientValidationError(){
        header("HTTP/1.1 400 Bad Request");
    }
}	
<?php
function getRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sara Future</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="<?php echo CSRF_TOKEN; ?>">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</head>
<body>

<div class="jumbotron text-center">
  <h1>SARA FUTURE</h1>
  <!-- <p>Resize this responsive page to see the effect!</p> --> 
</div>
  
<div class="container">
  <h2>User Course Details</h2>
  <button class="btn btn-primary pull-right mb-4" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModal">+ Add User</button>          
  <table class="table table-bordered" hidden id="dataContainer">
    <thead>
      <tr>
        <th>User Name</th>
        <th>Course Name</th>
        <th>Completed Date</th>
        <th>Time Spent (in minutes)</th>
        <th>Points</th>
        <th>Status (Passed/Failed)</th>
      </tr>
    </thead>
    <tbody id="dataBody">
    </tbody>
  </table>
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add User</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group ">
		    <label for="name">Name:</label>
		    <input type="text" class="form-control col-xs-12 col-sm-3 col-md-3" id="name" required>
	    </div>
	    <div class="form-group">
		    <label for="user_name">User Name:</label>
		    <input type="text" class="form-control" id="user_name" required>
	    </div>
	    <div class="form-group">
	       <label for="pwd">Password:</label>
	       <input type="password" class="form-control" id="pwd" required>
	    </div>
        <div class="form-group">
	       <label for="confirm_pwd">Confirm Password:</label>
	       <input type="password" class="form-control" id="confirm_pwd" required>
	    </div>
        <div class="form-group">
		    <label for="gender" class="control-label input-group">Gender:</label>
		    <div class="btn-group" data-toggle="buttons">
		        <label class="btn btn-default">
		            <input type="radio" name="gender" value="Male">Male
		        </label>
		        <label class="btn btn-default">
		            <input type="radio" name="gender" value="Female">Female
		        </label>
		    </div>
		</div>
		<div class="form-group">
	       <label for="joined_date">Joined Date:</label>
			<div class="input-group date" data-provide="datepicker">
			    <input type="text" class="form-control" id="joined_date" name="joined_date">
			    <div class="input-group-addon">
			        <span class="glyphicon glyphicon-th"></span>
			    </div>
			</div>
        </div>
        

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-submit">Sumbit</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('document').ready(function(){
	$('#joined_date').datepicker({
		format: 'yyyy-mm-dd',
		endDate: new Date()
	});
    $.ajax({
    	url: "<?php echo API_HOST ?>/getAllUserCourse",
    	dataType: 'json', 
    	success: function(result){
    		if(result.status == 'success'){
                var data = result.data;
                var html = '';
                $(data).each(function(i){
                    html += '<tr>';
                    html += '<td>'+data[i].user_name+'</td>';
                    html += '<td>'+data[i].course_name+'</td>';
                    html += '<td>'+data[i].completed_date+'</td>';
                    html += '<td>'+data[i].time_spent+'</td>';
                    html += '<td>'+data[i].points+'</td>';
                    html += '<td>'+data[i].status+'</td>';
                    html += '</tr>';
                });
                if(html){
                    $('#dataBody').empty().append(html);
                    $('#dataContainer').show();
                }
    		}else{
                 alert(result.message);
    		}
    	},
    	error: function(){}
    });
    $('body').on('click','#btn-submit',function(){
    	//$("input[name='gender']:checked"). val();
    	var regex_pattern = /^[a-z0-9 ]+$/i;
    	var pwd_regex_pattern = /^[a-z0-9]+$/i;
    	var name = $('#name');
    	var user_name = $('#user_name');
    	var pwd = $('#pwd');
    	var confirm_pwd = $('#confirm_pwd');
    	var gender = $("input[name='gender']");
    	var joined_date = $('#joined_date');
    	if(name.val() == '' || !regex_pattern.test(name.val())){
            name.closest('div.form-group').addClass('has-error');
            return;
    	}else{
    		name.closest('div.form-group').removeClass('has-error');
    	}

    	if(user_name.val() == '' || !regex_pattern.test(user_name.val())){
            user_name.closest('div.form-group').addClass('has-error');
            return;
    	}else{
    		user_name.closest('div.form-group').removeClass('has-error');
    	}

    	if(pwd.val() == '' || !pwd_regex_pattern.test(pwd.val())){
            pwd.closest('div.form-group').addClass('has-error');
            return;
    	}else{
    		pwd.closest('div.form-group').removeClass('has-error');
    	}

    	if(confirm_pwd.val() == '' || !pwd_regex_pattern.test(confirm_pwd.val()) || confirm_pwd.val() != pwd.val()){
            confirm_pwd.closest('div.form-group').addClass('has-error');
            return;
    	}else{
    		confirm_pwd.closest('div.form-group').removeClass('has-error');
    	}

    	if(!$("input[name='gender']:checked").val()){
            gender.closest('div.form-group').addClass('has-error');
            return;
    	}else{
    		gender.closest('div.form-group').removeClass('has-error');
    	}

    	if(joined_date.val() == ''){
            joined_date.closest('div.form-group').addClass('has-error');
            return;
    	}else{
    		joined_date.closest('div.form-group').removeClass('has-error');
    	}
    	var dataJson = {};
    	    dataJson.UserID = "<?php echo getRandomString(); ?>";
    	    dataJson.Name = name.val().trim();
    	    dataJson.Username = user_name.val().trim();
    	    dataJson.Password = pwd.val().trim();
    	    dataJson.Gender = $("input[name='gender']:checked"). val().trim();
    	    dataJson.JoinedDate = new Date(joined_date.val().trim()).getTime()/1000;
    	$.ajax({
    		type: 'POST',
	    	url: "<?php echo API_HOST ?>/user",
	    	dataType: 'json', 
	    	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: dataJson,
	    	success: function(result){
	    		if(result.status == 'success'){
	    			alert('User added successfully !');
	                location.reload(true);
	    		}else{
	                 alert(result.message);
	    		}
	    	},
	    	error: function(){}
	    });

    });

});	
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
</body>
</html>

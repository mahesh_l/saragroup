<?php
namespace App\Services;

use App\Models\DB;

class User extends API{
    
    public function __construct($request) {
        parent::__construct($request);
    }

    protected function addUser(){
        if ($this->method == 'POST') {
            if(!$this->validateNonEmpty($this->request)){
                return ['status'=>'error','message'=>'Mandatory fields are missing or empty'];
            }
            $data = $this->request;
            $data['Password'] = hash('sha256', $data['Password']);
            $db = DB::getInstance();
            $sql = "INSERT INTO user (user_id, name, user_name, password, gender, joined_date) VALUES (:UserID, :Name, :Username, :Password, :Gender, :JoinedDate)";
            $response = $db->executeQuery($sql, $data);
            if($response){
                return ['status'=>'success'];
            }else{
                return ['status'=>'error','message'=>$db->getError()];
            }
        }else{
            return "Only accepts POST requests";
        }
           
     }

     protected function addUserCourse(){
        if ($this->method == 'POST') {
            if(!$this->validateNonEmpty($this->request)){
                return ['status'=>'error','message'=>'Mandatory fields are missing or empty'];
            }
            
            $db = DB::getInstance();
            $sql = "INSERT INTO user_course (user_id, course_id) VALUES (:UserId, :CourseId)";
            $response = $db->executeQuery($sql, $this->request);
            if($response){
                return ['status'=>'success'];
            }else{
                return ['status'=>'error','message'=>$db->getError()];
            }
        }else{
            return "Only accepts POST requests";
        }
           
     }

     protected function updateUserCourse(){
        if ($this->method == 'POST') {
            if(!$this->validateNonEmpty($this->request)){
                return ['status'=>'error','message'=>'Mandatory fields are missing or empty'];
            }
            $data = $this->request;
            $db = DB::getInstance();
            $sql = "UPDATE user_course SET points =:Points, time_spent =:TimeSpent, completed_date =:CompletedDate, status =:Status WHERE (user_id =:UserId AND course_id =:CourseId)";
            $response = $db->executeQuery($sql, $data);
            if($response){
                return ['status'=>'success'];
            }else if($db->getError()){
                return ['status'=>'error','message'=>$db->getError()];
            }else{
                return ['status'=>'error','message'=>'Update failed. Please verify input data'];
            }
        }else{
            return "Only accepts POST requests";
        }
           
     }

     protected function getAllUser(){
        if ($this->method == 'GET') {
            $db = DB::getInstance();
            $sql = "SELECT * FROM user";
            $response = $db->executeQuery($sql, [], 'SELECT');
            if($response){
                return ['status'=>'success','data'=>$db->fetchData()];
            }else{
                return ['status'=>'error','message'=>$db->getError()];
            }
        }else{
            return "Only accepts GET requests";
        }
           
     }

     protected function getAllUserCourse(){
        if ($this->method == 'GET') {
            $db = DB::getInstance();
            $sql = "SELECT u.name as user_name, c.title as course_name, uc.completed_date, uc.time_spent, uc.points, uc.status FROM user_course uc INNER JOIN user u ON uc.user_id = u.user_id INNER JOIN course c ON uc.course_id = c.course_id";
            $response = $db->executeQuery($sql, [], 'SELECT');
            if($response){
                return ['status'=>'success','data'=>$db->fetchData()];
            }else{
                return ['status'=>'error','message'=>$db->getError()];
            }
        }else{
            return "Only accepts GET requests";
        }
           
     }

 }
 ?>
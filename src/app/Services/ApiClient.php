<?php

namespace App\Services;

class ApiClient{

	public static function User($method){
		try {
		    $api = new User($method);
		    echo $api->processApi();
		} catch (Exception $e) {
		    echo json_encode(Array('error' => $e->getMessage()));
		}
	}

	public static function Course($method){
		try {
		    $api = new Course($method);
		    echo $api->processApi();
		} catch (Exception $e) {
		    echo json_encode(Array('error' => $e->getMessage()));
		}
	}
}
?>
<?php

namespace App\Services;

abstract class API {
    protected $method = '';
    protected $endpoint = '';
    protected $verb = '';
    protected $args = Array();

    public function __construct($request) {
        // required headers
        header("Access-Control-Allow-Origin: http://localhost/saragroup/");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        $this->args = explode('/', rtrim($request, '/'));
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        
        switch($this->method) {
        case 'POST':
                $requestData = $this->cleanInputs($_POST);
                if(!empty($requestData)){
                    $this->request = $requestData;
                }else if(!empty(file_get_contents("php://input"))){
                    $json = file_get_contents("php://input");
                    $this->request = json_decode($json,true);
                }else{
                    $this->request = $this->cleanInputs($_POST);
                }
                break;
        case 'GET':
            $this->request = $this->cleanInputs($_GET);
            break;
        case 'PUT':
            //TODO
            break;
        default:
            $this->response('Invalid Method', 405);
            break;
        }
    }
    public function processApi() {
        if (method_exists($this, $this->endpoint)) {
            return $this->response($this->{$this->endpoint}($this->args));
        }
        return $this->response("No Endpoint: $this->endpoint", 404);
    }

    private function response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        header("Content-Type: application/json");
        echo json_encode($data);
    }

    private function cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            $clean_input = htmlspecialchars(trim(strip_tags($data)));
        }
        return $clean_input;
    }

    private function requestStatus($code) {
        $status = array(  
            200 => 'OK',
            404 => 'Not Found',   
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        ); 
        return ($status[$code])?$status[$code]:$status[500]; 
    }

    protected function validateNonEmpty($data, $optional=[]){
        foreach ($data as $key => $value) {
            if(empty($value) && !in_array($value, $optional)){
                return false;
                break;
            }
        }
        return true;
    }
}
?>
<?php
namespace App\Services;

use App\Models\DB;

class Course extends API{
    
    public function __construct($request) {
        parent::__construct($request);
    }

    protected function addCourse(){
        if ($this->method == 'POST') {
            if(!$this->validateNonEmpty($this->request)){
                return ['status'=>'error','message'=>'Mandatory fields are missing or empty'];
            }
            
            $db = DB::getInstance();
            $sql = "INSERT INTO course (course_id, title, description, number_of_points) VALUES (:CourseID, :Title, :Description, :NumberOfPoints)";
            $response = $db->executeQuery($sql, $this->request);
            if($response){
                return ['status'=>'success'];
            }else{
                return ['status'=>'error','message'=>$db->getError()];
            }
        }else{
            return "Only accepts POST requests";
        }
           
     }

 }
 ?>
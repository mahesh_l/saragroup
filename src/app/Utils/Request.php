<?php

namespace App\Utils;

class Request{
	
	public static function postParams(){
        if(!empty(file_get_contents("php://input"))){
            $json=file_get_contents("php://input");
            $reqArray =json_decode($json,true);
            return $reqArray;

        }else{
            return $_POST;
        }
	}
}
?>
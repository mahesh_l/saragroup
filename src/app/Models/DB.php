<?php
namespace App\Models;

use PDO;
use PDOException;

class DB {
    private static $instance = null;
    private $conn;
    private $query_result;
    private $error;
  
    const HOST = 'localhost';
    const PORT = '3306';
    const USER = 'root';
    const PASS = '';
    const DB_NAME = 'sara';
   
    private function __construct(){
        $dsn = "mysql:host=". self::HOST . ";port=" . self::PORT . ";dbname=" . self::DB_NAME ;
        $option  = array(PDO::ATTR_PERSISTENT => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
        try {
            $this->conn = new PDO($dsn, self::USER, self::PASS, $option);
        } catch (PDOException $e) {
            echo $e->getMessage();
            $this->error = $e->getMessage();
        }
    }

    public static function getInstance(){
        if(!self::$instance){
          self::$instance = new DB();
        }

        return self::$instance;
    }

    public function getConnection(){
        return $this->conn;
    }

    public function executeQuery($sql, $data = '', $type = ''){
        try {
            $db = self::getInstance();
            $pdo = $db->getConnection();
            $stmt = $pdo->prepare($sql);
            $stmt->execute($data);
            if($type == 'SELECT'){
                $this->query_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            $row_count = $stmt->rowCount();
            return $row_count;
        } catch (PDOException $e) {
            $error = $e->getMessage();
            if(strpos($error, ':') !== false){
                $errArray = explode(':', $error);
                if(strpos($errArray[2], '1062') !== false){
                    $this->error = 'Duplicate Entry';
                }else if(strpos($errArray[2], '1452') !== false){
                    $this->error = 'Both user and course need to be registered first';
                }elseif(!empty($errArray[2])){
                    $this->error = $errArray[2];
                }else{
                    $this->error = 'Internal server error occured. Please try after sometime.';
                }
            }
        }catch(\Throwable $e){
            //TODO: Logging
            $this->error = $e->getMessage();
        }
    }

    public function fetchData(){
        return $this->query_result;
    }

    public function getError(){
        return $this->error;
    }

    private function __clone() { 
    }
}

